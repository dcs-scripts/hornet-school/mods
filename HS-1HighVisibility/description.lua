livery = {

	{"f18c1", 0 ,"F18C_1_DIFF",false};
	{"f18c1", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",false};

	
	{"f18c2", 0 ,"F18C_2_DIFF",false};
	{"f18c2", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",false};
	
	
	{"AAQ_28", 0, "AAQ_28_104th_Grey_Diff", false};
	{"AAQ_28_Glass", 0, "AAQ_28_Glass_104", false};
   	{"f18c1_number_nose_right", 0 ,"F18C_1_DIFF",false};
    	{"f18c1_number_nose_right", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",false};
    	{"f18c1_number_nose_right", DECAL ,"F18C_bort_number_nose",false};
    
	{"f18c1_number_nose_left", 0 ,"F18C_1_DIFF",false};
	{"f18c1_number_nose_left", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",false};
	{"f18c1_number_nose_left", DECAL ,"F18C_bort_number_nose",false};
    
	{"f18c1_number_F", 0 ,"F18C_1_DIFF",false};
	{"f18c1_number_F", ROUGHNESS_METALLIC ,"F18C_1_DIF_RoughMet",false};
	{"f18c1_number_F", DECAL ,"F18C_bort_number2",false};

	{"f18c2_number_X", 0 ,"F18C_2_DIFF",false};
	{"f18c2_number_X", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",false};
	{"f18c2_number_X", DECAL ,"F18C_bort_number2",false};

	{"f18c2_kil_right", 0 ,"F18C_2_DIFF",false};
	{"f18c2_kil_right", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",false};
	{"f18c2_kil_right", DECAL ,"F18C_bort_number2",false};

	{"f18c2_kil_left", 0 ,"F18C_2_DIFF",false};
	{"f18c2_kil_left", ROUGHNESS_METALLIC ,"F18C_2_DIF_RoughMet",false};
	{"f18c2_kil_left", DECAL ,"F18C_bort_number2",false};

	{"FPU_8A", 0 ,"FPU_8A",false};
	{"FPU_8A", 2 ,"FPU_8A_Diff_RoughMet",true};
	
	{"pilot_F18", 0, "pilot_f18", true};
	{"pilot_F18_helmet", 0 ,"pilot_F18_helmet",true};
	{"pilot_F18_patch", 0, "empty", true};



	
}
name = "HS-1 High Visibility"

custom_args = 
{
--Set to 1.0 to make number NOT appear 

[27] = 0.0, -- Does Nothing For me (supposedly controls upper tail for some)
[1000] = 0.0, -- Flaps
[1001] = 0.0, -- Upper Nose Nose
[1002] = 1.0, -- Large Tail and Lower Nose
[1003] = 1.0, -- Rear Fuselage & Gear Doors Two Numbers
[1004] = 1.0, -- Mid Fuselage Two Numbers
[1005] = 1.0, -- Tail Small
[1006] = 1.0, -- Blue Angels (does nothing for me)

}